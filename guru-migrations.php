<?php
/**
 * Plugin Name: Guru Migrations
 * Description: Guru Migrations
 * Version: 1.0.0
 * Author: Mindaugas
 */

if ( !defined( 'ABSPATH' ) ) {
    die;
}

define( 'Guru_Migrations_FILE', __FILE__ );
define( 'Guru_Migrations_PATH', plugin_dir_path( Guru_Migrations_FILE ) );
define( 'Guru_Migrations_URL', plugin_dir_url( Guru_Migrations_FILE ) );

register_deactivation_hook( Guru_Migrations_FILE, array( 'Guru_Migrations', 'guru_migrations_deactivate' ) );

final class Guru_Migrations
{

    /**
     * Plugin instance.
     *
     * @var Guru_Migrations
     * @access private
     */
    private static $instance = null;

    /**
     * Get plugin instance.
     *
     * @return Guru_Migrations
     * @static
     */
    public static function get_instance()
    {
        if ( ! isset(self::$instance) ) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    /**
     * Guru_Migrations constructor.
     */
    private function __construct()
    {
        register_activation_hook(Guru_Migrations_FILE, array($this , 'guru_migrations_activate'));

        $this->guru_migrations_includes();

        add_action('admin_init', array($this, 'migrate'));
    }

    public function migrate()
    {
        $migrations_folder = WP_CONTENT_DIR . '/migrations';
        $src_directory = new RecursiveDirectoryIterator($migrations_folder);

        $filtered_file_list = new GuruFilenameFilter($src_directory, '/(?:php)$/i');
        $filtered_file_list = new GuruFilenameFilter($filtered_file_list, '/^(?!.*(Complex|Exception)\.php).*$/i');

        foreach (new RecursiveIteratorIterator($filtered_file_list) as $file) {
            if ($file->isFile()) {
                include_once $file;

                $option = $guru_migration_date_updated . '_' . pathinfo($file->getFileName(), PATHINFO_FILENAME);
                $old_options = get_option('guru_migrations', []);
                if (isset($old_options[$option])) continue;

                $guru_migration_call();
                $this->guru_migrations_update_option($option);
            }
        }
    }

    public function guru_migrations_update_option($key)
    {
        $old_options = get_option('guru_migrations', []);

        $old_options[$key] = true;

        update_option('guru_migrations', $old_options);
    }

    /**
     * Run when deactivate plugin.
     */
    public static function guru_migrations_deactivate()
    {
        require_once Guru_Migrations_PATH . 'includes/guru-migrations-deactivator.php';
        Guru_Migrations_Deactivator::deactivate();
    }

    /**
     * Run when activate plugin.
     */
    public function guru_migrations_activate()
    {
        require_once Guru_Migrations_PATH . 'includes/guru-migrations-activator.php';
        Guru_Migrations_Activator::activate();
    }

    /**
     * Loading plugin functions files
     */
    public function guru_migrations_includes()
    {
        require_once Guru_Migrations_PATH . 'includes/guru-migrations-functions.php';
    }
}

function Guru_Migrations()
{
    return Guru_Migrations::get_instance();
}

$GLOBALS['Guru_Migrations'] = Guru_Migrations();
