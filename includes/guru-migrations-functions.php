<?php

abstract class GuruFilesystemRegexFilter extends RecursiveRegexIterator
{
    protected $regex;

    public function __construct(RecursiveIterator $it, $regex)
    {
        $this->regex = $regex;
        parent::__construct($it, $regex);
    }
}

class GuruFilenameFilter extends GuruFilesystemRegexFilter
{
    // Filter files against the regex
    public function accept()
    {
        return (!$this->isFile() || preg_match($this->regex, $this->getFilename()));
    }
}
